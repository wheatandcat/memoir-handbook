# 達成したタスク

## 目的

達成したタスクを振り返れるように登録できるようにする。<br/>
また、登録した達成したタスクの振り返りがしやすいように複数の入力項目を設けている。


## 入力項目

|  項目名  | 内容  | 入力必須 |
| ---- | ---- | ---- |
|  日付  | 達成したタスクが発生した日 | ◯ |
|  本文  | 達成したタスクの内容のテキスト | ◯ |
|  カテゴリー  | 達成したタスクのカテゴリーを設定<br/>詳細は、こちらを参照 | ◯ |
|  Good  | 達成したタスクが良い出来事だった | ✕ |
|  Bad  | 達成したタスクが悪い出来事だった | ✕ |

## カテゴリー

以下のカテゴリーが存在する。

|  項目名  | 画像 |
| ---- | ---- |
|  生活全般  | <img src="../../img/category_aquarium@2x.png" width="40"/> |
|  飲食した  | <img src="../../img/category_food@2x.png" width="40"/> |
|  健康関連  | <img src="../../img/category_heart@2x.png" width="40"/> |
|  外出した  | <img src="../../img/category_door_close@2x.png" width="40"/> |
|  趣味全般  | <img src="../../img/category_dancing@2x.png" width="40"/> |
|  鑑賞した  | <img src="../../img/category_musical@2x.png" width="40"/> |
|  勉強した  | <img src="../../img/category_study@2x.png" width="40"/> |
|  買った  | <img src="../../img/category_shopping@2x.png" width="40"/> |
|  特別なこと  | <img src="../../img/category_specialstar@2x.png" width="40"/> |
